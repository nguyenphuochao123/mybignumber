<?php
class MyBigNumber {
    public function sum($stn1, $stn2) {
        // Khởi tạo biến để lưu kết quả và biến nhớ
        $result = "";
        $carry = 0;
        
        // Duyệt chuỗi từ phải sang trái
        for ($i = strlen($stn1) - 1, $j = strlen($stn2) - 1; $i >= 0 || $j >= 0; $i--, $j--) {
            // Lấy ra kí số tại vị trí i của chuỗi stn1 và kí số tại vị trí j của chuỗi stn2 (nếu i hoặc j đã hết chuỗi, ta lấy kí số bằng 0)
            $digit1 = $i >= 0 ? intval($stn1[$i]) : 0;
            $digit2 = $j >= 0 ? intval($stn2[$j]) : 0;
            
            // Cộng hai kí số và biến nhớ, lưu kết quả vào biến sum
            $sum = $digit1 + $digit2 + $carry;
            
            // Tính giá trị của biến nhớ bằng cách chia tổng sum cho 10 và lấy phần nguyên
            $carry = intdiv($sum, 10);
            
            // Lưu giá trị sum % 10 vào đầu chuỗi result
            $result = ($sum % 10) . $result;
        }
        
        // Nếu biến nhớ còn lớn hơn 0 thì thêm giá trị của biến nhớ vào đầu chuỗi result
        if ($carry > 0) {
            $result = $carry . $result;
        }
        
        // Trả về chuỗi kết quả
        return $result;
    }
}
$kq=new MyBigNumber();
echo $kq->sum("999","999");
