# MyBigNumber
1.Khởi tạo biến $result và $carry để lưu kết quả và giá trị nhớ, đều ban đầu bằng 0.
2.Duyệt hai chuỗi số từ phải sang trái bằng vòng lặp for:
a. Lấy ra kí số tại vị trí i của chuỗi stn1 và kí số tại vị trí j của chuỗi stn2 (nếu i hoặc j đã hết chuỗi, ta lấy kí số bằng 0).
b. Tính tổng hai kí số và giá trị của biến nhớ, lưu kết quả vào biến sum.
c. Tính giá trị của biến nhớ bằng cách chia tổng sum cho 10 và lấy phần nguyên.
d. Lưu giá trị sum % 10 vào đầu chuỗi kết quả result.
3.Nếu giá trị của biến nhớ còn lớn hơn 0, ta thêm giá trị của biến nhớ vào đầu chuỗi kết quả.
4.Trả về chuỗi kết quả.

## Hướng dẫn cài đặt và sử dụng

1. Cài đặt môi trường:Cài wamp server hoặc xampp để chạy môi trường php localhost
2. Tải về source code từ GitHub


